const settings = require('ep_etherpad-lite/node/utils/Settings');

if (settings.ep_matomo) {
    var matomoSettings = true;
    if (settings.ep_matomo.url) {
        var matomoUrl = settings.ep_matomo.url;
    } else {
        var matomoUrl = false;
    }

    if (settings.ep_matomo.siteId) {
        var matomoSiteId = settings.ep_matomo.siteId;
    } else {
        var matomoSiteId = false;
    }

    if (settings.ep_matomo.indexSiteId) {
        var matomoIndexSiteId = settings.ep_matomo.indexSiteId;
    } else {
        var matomoIndexSiteId = matomoSiteId;
    }
} else {
    var matomoSettings = false;
}

exports.eejsBlock_scripts = function (hook_name, args, cb) {
    const snippet = generateSnippet(matomoSiteId);
    if (snippet) {
        args.content += snippet;
    }
    return cb();
};

exports.eejsBlock_indexCustomScripts = function (hook_name, args, cb) {
    const snippet = generateSnippet(matomoIndexSiteId);
    if (snippet) {
        args.content += snippet;
    }
    return cb();
};

function generateSnippet(matomoSiteId) {
    if (!matomoSettings) {
        console.warn('ep_matomo not set in settings.json, insert it in /admin/settings');
    } else if (!matomoUrl) {
        console.warn('ep_matomo.url not set in settings.json, insert it in /admin/settings');
    } else if (!matomoSiteId) {
        console.warn('ep_matomo.siteId not set in settings.json, insert it in /admin/settings');
    } else {
        var matomoString = "<script type='text/javascript'>var _paq = _paq || [];_paq.push(['trackPageView']);_paq.push(['enableLinkTracking']);(function() {var u=\"//" + matomoUrl + "/\";_paq.push(['setTrackerUrl', u+'matomo.php']);_paq.push(['setSiteId', " + matomoSiteId + "]);var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);})();</script>";
        matomoString += "<noscript><p><img src='//" + matomoUrl + "/matomo.php?idsite=" + matomoSiteId + "'style='border:0;' alt='' /></p></noscript>";
        return matomoString; // add matomo to the contents
    }
    return undefined;
}