Etherpad plugin to insert Matomo analytics code inside a pad.

This is a fork from
[ep_piwik](https://github.com/alx/ep_piwik)
by [Alexandre Girard](https://github.com/alx).


# Configuration
Add these settings inside your *settings.json* :
```
  "ep_matomo": {
    "url": "matomo.website.com",
    "siteId": "1234",
    "indexSiteId": "abc"
  }
```

* *url* : this is the website where you've installed your Matomo
* *siteId* : this is the number given inside Matomo administration to the
website you want to track
* *indexSiteId*: special siteId to be used on the index page, defaults to *siteId*